using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishLine : MonoBehaviour
{
    [SerializeField] public float delayTime = 0.5f;
    [SerializeField] ParticleSystem finishEffect;
     

    // Triggers Level/Game over if player crosses finish-Line
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            // This plays the particle effect when this 'if' statement works
            finishEffect.Play();
            // Add SFX for crossing FinishLine
            GetComponent<AudioSource>().Play();
            Invoke("ReloadScene", delayTime);
        }
    }

    private void ReloadScene() 
    {
        SceneManager.LoadScene(0);
        
    }


}
