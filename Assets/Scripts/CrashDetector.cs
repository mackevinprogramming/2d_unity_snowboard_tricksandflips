using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CrashDetector : MonoBehaviour
{
    // invoke delay time var
    [SerializeField] float crashDelayTime = 0.5f;
    [SerializeField] ParticleSystem crashEffect;
    [SerializeField] AudioClip crashSFX;
    bool hasCrashed;

    // If "Head" (i.e. CircleCollider2D) triggers with "Ground" tag (from Mountain) Do something i.e. Restart Level, WriteToConsole...
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ground")
        {
            // Gets 'DisableControls' Method from "PlayerController" to stop player from moving if Player crashes 
            FindObjectOfType<PlayerController>().DisableControls();

            if (!hasCrashed && collision.tag == "Ground")
            {
                // hasCrashed turns off the crashEffect and Sound so, it doesn't double play both of those
                hasCrashed = true;
                crashEffect.Play();
                // Implementing CrashSFX
                GetComponent<AudioSource>().PlayOneShot(crashSFX);
                
            }
            
            
            Invoke("ReloadScene", crashDelayTime);
        }
    }


    void ReloadScene()
    {
        SceneManager.LoadScene(0);
    }

    

}
