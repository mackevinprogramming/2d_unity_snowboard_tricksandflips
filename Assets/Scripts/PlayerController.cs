using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D rb2d;
    [SerializeField] float boostSpeed = 30.0f;
    [SerializeField] float baseSpeed = 15.0f;

    [SerializeField] float torqueAmmount = 1.0f;
    SurfaceEffector2D surfaceEffector2D;

    bool canMove = true;



    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        // This is for finding the Component 'SurfaceEffector' to be used by the PlayerController
        surfaceEffector2D = FindObjectOfType<SurfaceEffector2D>();
    }

    // Start is called before the first frame update
    private void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        if (canMove)
        {
            RotatePlayer();
            RespondToBoost();
        }
    }

    public void DisableControls() 
    {
        // Changes canMove bool and now Player can Rotate or Boost
        canMove = false;
    }

    private void RespondToBoost()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            surfaceEffector2D.speed = boostSpeed;
        }
        else
        {
            surfaceEffector2D.speed = baseSpeed;
        }
    }

    private void RotatePlayer()
    {

        // This is having player rotate into a backflip 'towards the left'
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb2d.AddTorque(torqueAmmount);
        }
        // Player rotates into a flip 'towards the right'
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            rb2d.AddTorque(-torqueAmmount);
        }


    }
}
